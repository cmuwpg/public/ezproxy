FROM debian:buster-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    curl ca-certificates libnss-sss libpam-sss git gettext

RUN mkdir -p /usr/local/ezproxy \
    && curl -L -o /usr/local/ezproxy/ezproxy 'https://help.oclc.org/@api/deki/files/9850/ezproxy-linux.bin' \
    && chmod 0755 /usr/local/ezproxy/ezproxy

ENTRYPOINT ["/usr/local/ezproxy/ezproxy", "-d", "/var/lib/ezproxy"]
